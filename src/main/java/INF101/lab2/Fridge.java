package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    //field variables
    int maxSize = 20;

    //Constructor
    
    //method total fridge size
    public int totalSize() {
        return maxSize;
    }

    //create empty list. Will be filled with fride items.
    List<FridgeItem> listFridgeItems = new ArrayList<>();
    

    @Override
    public int nItemsInFridge() {

        int sizeOfFridge = listFridgeItems.size();
        return sizeOfFridge;
    }

    @Override
    public boolean placeIn(FridgeItem item) {

        if (nItemsInFridge() < totalSize()) {
            listFridgeItems.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        
        if(listFridgeItems.contains(item)) {
            listFridgeItems.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        listFridgeItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> listExpiredFood = new ArrayList<>();

        for (FridgeItem item : listFridgeItems) {
            if (item.hasExpired()) {
                listExpiredFood.add(item);
            }
        }
        listFridgeItems.removeAll(listExpiredFood);

        return listExpiredFood;
    }
}
